export const skills = [
  {
    category: "Markup Language",
    name: "HTML5",
  },
  {
    category: "Styling Language",
    name: "CSS3",
  },
  {
    category: "Programming Language",
    name: "JavaScript",
  },
  {
    category: "CSS Framework",
    name: "Tailwind CSS",
  },
  {
    category: "JavaScript Library",
    name: "React.js",
  },
  {
    category: "React Framework",
    name: "Next.js",
  },
  {
    category: "Backend JavaScript Runtime",
    name: "NodeJS",
  },
  {
    category: "Structuring Data",
    name: "JSON",
  },
  {
    category: "Version Control (VCS)",
    name: "Git",
  },
  {
    category: "Source Code Management",
    name: "GitHub",
  },
  {
    category: "Design Tool",
    name: "Adobe XD",
  },
];
